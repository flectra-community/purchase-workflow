# Copyright 2016 Chafique DELLI @ Akretion
# License AGPL-3 - See http://www.gnu.org/licenses/agpl-3.0.html

{
    "name": "Purchase Picking State",
    "version": "2.0.1.1.0",
    "author": "Akretion, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "category": "Purchase Management",
    "website": "https://gitlab.com/flectra-community/purchase-workflow",
    "depends": ["purchase", "purchase_stock"],
    "data": ["views/purchase_view.xml"],
    "installable": True,
}
