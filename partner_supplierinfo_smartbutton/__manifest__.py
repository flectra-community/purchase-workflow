# Copyright 2022 Tecnativa - Víctor Martínez
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "Access supplied products from the vendor",
    "version": "2.0.1.0.0",
    "category": "Purchase Management",
    "website": "https://gitlab.com/flectra-community/purchase-workflow",
    "author": "Tecnativa, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "depends": ["product"],
    "installable": True,
    "data": [
        "views/res_partner_view.xml",
    ],
    "maintainers": ["victoralmau"],
}
