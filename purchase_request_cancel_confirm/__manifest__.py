# Copyright 2020 Ecosoft Co., Ltd. (http://ecosoft.co.th)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Purchase Request Cancel Confirm",
    "version": "2.0.1.0.0",
    "author": "Ecosoft,Odoo Community Association (OCA)",
    "category": "Usability",
    "license": "AGPL-3",
    "website": "https://gitlab.com/flectra-community/purchase-workflow",
    "depends": ["base_cancel_confirm", "purchase_request"],
    "auto_install": False,
    "installable": True,
    "maintainers": ["kittiu"],
}
