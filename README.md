# Flectra Community / purchase-workflow

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[purchase_rfq_number](purchase_rfq_number/) | 2.0.1.0.0| Different sequence for purchase for quotations
[product_supplier_code_purchase](product_supplier_code_purchase/) | 2.0.1.0.0| This module adds to the purchase order line the supplier                code defined in the product.
[purchase_order_approval_block](purchase_order_approval_block/) | 2.0.1.0.2| Purchase Order Approval Block
[purchase_product_usage](purchase_product_usage/) | 2.0.1.1.0| Purchase Product Usage
[purchase_deposit](purchase_deposit/) | 2.0.1.1.1| Option to create deposit from purchase order
[purchase_order_type](purchase_order_type/) | 2.0.1.0.2| Purchase Order Type
[purchase_tier_validation](purchase_tier_validation/) | 2.0.2.0.1| Extends the functionality of Purchase Orders to support a tier validation process.
[purchase_open_qty](purchase_open_qty/) | 2.0.1.1.0| Allows to identify the purchase orders that have quantities pending to invoice or to receive.
[purchase_order_line_price_history](purchase_order_line_price_history/) | 2.0.1.0.0| Purchase order line price history
[purchase_tag](purchase_tag/) | 2.0.1.0.0| Allows to add multiple tags to purchase orders
[purchase_request_type](purchase_request_type/) | 2.0.1.0.1| Purchase Request Type
[purchase_advance_payment](purchase_advance_payment/) | 2.0.1.0.1| Allow to add advance payments on purchase orders
[purchase_order_line_packaging_qty](purchase_order_line_packaging_qty/) | 2.0.1.0.1| Define quantities according to product packaging on purchase order lines
[product_form_purchase_link](product_form_purchase_link/) | 2.0.1.0.0|         Add an option to display the purchases lines from product
[purchase_manual_currency](purchase_manual_currency/) | 2.0.1.0.0| Allows to manual currency of Purchase
[purchase_blanket_order](purchase_blanket_order/) | 2.0.1.0.2| Purchase Blanket Orders
[purchase_requisition_tier_validation](purchase_requisition_tier_validation/) | 2.0.1.0.0| Extends the functionality of Purchase Agreements to support a tier validation process.
[purchase_request_department](purchase_request_department/) | 2.0.1.0.0| Purchase Request Department
[purchase_order_line_invoicing](purchase_order_line_invoicing/) | 2.0.1.0.0|         Allows to invoice lines of multiple purchase orders
[purchase_representative](purchase_representative/) | 2.0.1.0.0| Purchase Representatives will be the point of contact for RFQ's and PO's
[purchase_work_acceptance_invoice_plan](purchase_work_acceptance_invoice_plan/) | 2.0.1.0.1| Purchase Work Acceptance Invoice Plan
[purchase_picking_state](purchase_picking_state/) | 2.0.1.1.0| Purchase Picking State
[purchase_work_acceptance_late_fines](purchase_work_acceptance_late_fines/) | 2.0.1.0.0| Purchase Work Acceptance - Late Delivery Fines
[purchase_request_exception](purchase_request_exception/) | 2.0.1.0.0| Custom exceptions on purchase request
[procurement_batch_generator](procurement_batch_generator/) | 2.0.1.0.0| Wizard to replenish from product tree view
[purchase_order_line_deep_sort](purchase_order_line_deep_sort/) | 2.0.1.0.0| Purchase Order Line Sort
[purchase_order_line_menu](purchase_order_line_menu/) | 2.0.1.0.0| Adds Purchase Order Lines Menu
[purchase_manual_delivery](purchase_manual_delivery/) | 2.0.1.0.0|         Prevents pickings to be auto generated upon Purchase Order confirmation        and adds the ability to manually generate them as the supplier confirms        the different purchase order lines.    
[purchase_request_cancel_confirm](purchase_request_cancel_confirm/) | 2.0.1.0.0| Purchase Request Cancel Confirm
[purchase_order_approved](purchase_order_approved/) | 2.0.1.1.0| Add a new state 'Approved' in purchase orders.
[purchase_commercial_partner](purchase_commercial_partner/) | 2.0.1.0.0| Add stored related field 'Commercial Supplier' on POs
[procurement_purchase_no_grouping](procurement_purchase_no_grouping/) | 2.0.1.0.1| Procurement Purchase No Grouping
[purchase_invoice_plan](purchase_invoice_plan/) | 2.0.1.3.1| Add to purchases order, ability to manage future invoice plan
[purchase_propagate_qty](purchase_propagate_qty/) | 2.0.1.0.2| Quantity decrease on purchase line are propagated to the corresponding stock.move
[purchase_order_line_sequence](purchase_order_line_sequence/) | 2.0.1.0.0| Adds sequence to PO lines and propagates it toInvoice lines and Stock Moves
[purchase_stock_secondary_unit](purchase_stock_secondary_unit/) | 2.0.1.0.0| Get product quantities in a secondary unit
[purchase_request_to_requisition](purchase_request_to_requisition/) | 2.0.1.0.0| Purchase Request to Purchase Agreement
[vendor_transport_lead_time](vendor_transport_lead_time/) | 2.0.1.0.1| Purchase delay based on transport and supplier delays
[purchase_report_menu_move](purchase_report_menu_move/) | 2.0.1.0.0| Purchase Report Menu Move
[purchase_reception_status](purchase_reception_status/) | 2.0.1.0.0| Add reception status on purchase orders
[purchase_minimum_amount](purchase_minimum_amount/) | 2.0.1.0.1| Purchase Minimum Amount
[purchase_isolated_rfq](purchase_isolated_rfq/) | 2.0.1.0.1| Purchase Isolated RFQ
[purchase_request](purchase_request/) | 2.0.1.3.5| Use this module to have notification of requirements of materials and/or external services and keep track of such requirements.
[partner_supplierinfo_smartbutton](partner_supplierinfo_smartbutton/) | 2.0.1.0.0| Access supplied products from the vendor
[purchase_cancel_confirm](purchase_cancel_confirm/) | 2.0.1.0.0| Purchase Cancel Confirm
[purchase_triple_discount](purchase_triple_discount/) | 2.0.1.0.1| Manage triple discount on purchase order lines
[purchase_cancel_reason](purchase_cancel_reason/) | 2.0.1.0.0| Purchase Cancel Reason
[purchase_lot](purchase_lot/) | 2.0.1.0.0| Purchase Lot
[purchase_partner_incoterm](purchase_partner_incoterm/) | 2.0.2.0.0| Add a an incoterm field for supplier and use it on purchase order
[purchase_security](purchase_security/) | 2.0.1.0.0| See only your purchase orders
[purchase_order_secondary_unit](purchase_order_secondary_unit/) | 2.0.2.0.1| Purchase product in a secondary unit
[purchase_reception_notify](purchase_reception_notify/) | 2.0.1.0.0| Purchase Reception Notify
[purchase_force_invoiced](purchase_force_invoiced/) | 2.0.1.0.0| Allows to force the billing status of the purchase order to "Invoiced"
[purchase_order_line_description_picking](purchase_order_line_description_picking/) | 2.0.1.0.0| Purchase Order Line Name To Picking
[purchase_order_uninvoiced_amount](purchase_order_uninvoiced_amount/) | 2.0.1.1.0| Show uninvoiced amount on purchase order tree.
[purchase_exception](purchase_exception/) | 2.0.1.0.0| Custom exceptions on purchase order
[purchase_work_acceptance](purchase_work_acceptance/) | 2.0.1.2.0| Purchase Work Acceptance
[purchase_location_by_line](purchase_location_by_line/) | 2.0.1.0.1| Allows to define a specific destination location on each PO line
[purchase_partner_approval](purchase_partner_approval/) | 2.0.1.0.0| Control Partners that can be used in Purchase Orders
[purchase_discount](purchase_discount/) | 2.0.1.1.1| Purchase order lines with discounts
[purchase_order_archive](purchase_order_archive/) | 2.0.1.0.0| Archive Purchase Orders
[purchase_quick](purchase_quick/) | 2.0.1.0.1| Quick Purchase order
[purchase_fop_shipping](purchase_fop_shipping/) | 2.0.1.1.0| Purchase Free-Of-Payment shipping
[purchase_analytic_global](purchase_analytic_global/) | 2.0.1.0.0| Purchase - Analytic Account Global
[purchase_order_price_recalculation](purchase_order_price_recalculation/) | 2.0.1.0.0| Price recalculation in purchases orders
[purchase_request_tier_validation](purchase_request_tier_validation/) | 2.0.2.1.0| Extends the functionality of Purchase Requests to support a tier validation process.
[purchase_order_product_attachment_mgmt](purchase_order_product_attachment_mgmt/) | 2.0.1.0.0| Purchase Order Product Attachment Mgmt
[purchase_last_price_info](purchase_last_price_info/) | 2.0.2.0.1| Purchase Product Last Price Info
[purchase_order_vendor_product](purchase_order_vendor_product/) | 2.0.1.1.0| Show only products from the selected vendor in PO line
[purchase_work_acceptance_evaluation](purchase_work_acceptance_evaluation/) | 2.0.2.0.0| Purchase Work Acceptance Evaluation
[purchase_delivery_split_date](purchase_delivery_split_date/) | 2.0.1.1.1| Allows Purchase Order you confirm to generate one Incoming Shipment for each expected date indicated in the Purchase Order Lines


