# Copyright 2021 Ecosoft Co., Ltd. (http://ecosoft.co.th)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Purchase Report Menu Move",
    "version": "2.0.1.0.0",
    "author": "Ecosoft,Odoo Community Association (OCA)",
    "category": "Purchase",
    "license": "AGPL-3",
    "website": "https://gitlab.com/flectra-community/purchase-workflow",
    "depends": ["purchase"],
    "data": ["report/purchase_report_views.xml"],
    "auto_install": False,
    "installable": True,
    "maintainers": ["newtratip"],
}
