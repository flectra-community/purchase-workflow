# Copyright 2017-2020 Onestein (<https://www.onestein.eu>)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "Purchase Order Archive",
    "summary": "Archive Purchase Orders",
    "author": "Onestein, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/purchase-workflow",
    "category": "Purchases",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "depends": ["purchase"],
    "data": ["views/purchase_order.xml"],
    "installable": True,
}
