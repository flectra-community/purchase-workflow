{
    "name": "Purchase Order Vendor Products",
    "version": "2.0.1.1.0",
    "summary": "Show only products from the selected vendor in PO line",
    "author": "Ooops, Cetmix, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "category": "Purchase",
    "website": "https://gitlab.com/flectra-community/purchase-workflow",
    "depends": ["purchase_stock"],
    "external_dependencies": {},
    "demo": [],
    "data": [
        "views/purchase_order.xml",
    ],
    "qweb": [],
    "installable": True,
    "application": False,
}
