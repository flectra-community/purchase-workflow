# Copyright 2022 Tecnativa - Víctor Martínez
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).
{
    "name": "Purchase Order Product Attachment Mgmt",
    "version": "2.0.1.0.0",
    "category": "Purchases",
    "website": "https://gitlab.com/flectra-community/purchase-workflow",
    "author": "Tecnativa, Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "depends": ["purchase"],
    "installable": True,
    "data": [
        "views/purchase_order_view.xml",
    ],
    "maintainers": ["victoralmau"],
}
